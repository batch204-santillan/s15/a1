/*
1. In the S15 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.
4. Create variables to store to the following user details:
    - first name - String
    - last name - String
    -  age - Number
    - hobbies - Array
    - work address - Object
5. The hobbies array should contain at least 3 hobbies as Strings.
6. The work address object should contain the following key-value pairs:
      houseNumber: <value>
      street: <value>
      city: <value>
      state: <value>
7. Log the values of each variable to follow/mimic the output.
8. Identify and implement the best practices of creating and using variables by avoiding errors and debugging the following codes. 
      - Log the values of each variable to follow/mimic the output.
9. Create a git repository named S15.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.*/


// CHECKLIST

/*let fullName = "Steve Rogers";
	console.log("My full name is" + name);*/

/*let age = 40;
console.log("My current age is: " + currentAge);*/

//hobbies

//work address

/*let friends = ["Tony","Bruce",Thor,Natasha,"Clint""Nick"];
console.log("My Friends are: ")
console.log(friends);*/

/*let profile = {

	username "captain_america"
	fullName: "Steve Rogers';
	age: 40,
	isActive: false,

}
console.log("My Full Profile: ")
console.log(profile);*/

/*let fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);*/

/*const lastLocation = "Arctic Ocean";
lastLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);*/

//ALL DONE!


